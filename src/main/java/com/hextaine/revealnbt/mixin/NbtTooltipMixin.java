package com.hextaine.revealnbt.mixin;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.List;

@Mixin(ItemStack.class)
// Include implementing of the interface to keep consistency
public abstract class NbtTooltipMixin {

    @Inject(method="getTooltip", at=@At("RETURN"), cancellable = true)
    private void getTooltip(PlayerEntity player, TooltipContext context, CallbackInfoReturnable<List<Text>> ci) {
        List<Text> toolTip = ci.getReturnValue();
        List<Text> toolTip2 = new ArrayList<>(toolTip);

        for (Text t : toolTip) {
            if (t instanceof TranslatableText) {
                if (((TranslatableText)t).getKey().equals("item.nbt_tags")) {
                    // Dealing with Text that shows NBT information
                    int index = toolTip2.indexOf(t);
                    if (index < 0 ) continue; // Somehow the tag didn't exist in List2? Just move on and die inside...

                    if (this.getTag() != null) {
                        toolTip2.set(index, new LiteralText("NBT: {").formatted(Formatting.DARK_GRAY));

                        Text rpl = this.getTag().toText("  ", 0);
                        for (String line : rpl.asFormattedString().split("\n")) {
                            if (line.trim().equals("{")) continue;

                            line = line.replaceAll("§.", "");
                            toolTip2.add(new LiteralText(line).formatted(Formatting.DARK_GRAY));
                        }
                    }
                }
            }
        }
        ci.setReturnValue(toolTip2);
    }

    @Shadow
    private CompoundTag getTag() { return null; }

    private Text formatTag(Text previous, int depth, Tag tag) {
        if (previous != null) previous = previous.append("\n");
        else previous = new LiteralText("");

        previous = tag.toText("\t", depth);

        return previous;
        /*
        switch(tag.getType()) {
            case 0:
//                return new EndTag();
            case 1:
//                return new ByteTag();
            case 2:
//                return new ShortTag();
            case 3:
//                return new IntTag();
            case 4:
//                return new LongTag();
            case 8:
//                return new StringTag();
            case 5:
//                return new FloatTag();
            case 6:
//                return new DoubleTag();
            case 7:
//                return new ByteArrayTag();
            case 9:
//                return new ListTag();
            case 10:
//                return new CompoundTag();
            case 11:
//                return new IntArrayTag();
            case 12:
//                return new LongArrayTag();
            default:
                return null;
        }
         */
    }
}
