package com.hextaine.revealnbt;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import net.fabricmc.fabric.api.client.keybinding.FabricKeyBinding;
import net.fabricmc.fabric.api.client.keybinding.KeyBindingRegistry;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.network.MessageType;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import org.lwjgl.glfw.GLFW;

public class ExpandItemData {
    private static FabricKeyBinding keyBinding;
    private static boolean pressed = false;
    private static int pressedCD = 10;

    public void init() {
        keyBinding = FabricKeyBinding.Builder.create(
                new Identifier("reveal_nbt", "log"),
                InputUtil.Type.KEYSYM,
                GLFW.GLFW_KEY_LEFT_ALT,
                "Reveal NBT"
        ).build();

        KeyBindingRegistry.INSTANCE.register(keyBinding);
        
        ClientTickCallback.EVENT.register(e -> {
            if(keyBinding.isPressed()) {
                if (pressed) return;
                pressed = true;

                System.out.println(e.player.inventory.getCursorStack());
                e.inGameHud.addChatMessage(MessageType.CHAT, new LiteralText("Hello!"));
            } else {
                pressed = false;
            }
        });
    }
}
